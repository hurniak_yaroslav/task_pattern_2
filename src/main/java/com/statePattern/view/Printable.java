package com.statePattern.view;

@FunctionalInterface
public interface Printable {
    void print();
}
