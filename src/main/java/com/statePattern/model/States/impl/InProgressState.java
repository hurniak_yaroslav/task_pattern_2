package com.statePattern.model.States.impl;

import com.statePattern.model.States.State;
import com.statePattern.model.Task;

public class InProgressState extends State {
    @Override
    public void toReview(Task task) {
        task.setState(new CodeReviewState());
    }
}
