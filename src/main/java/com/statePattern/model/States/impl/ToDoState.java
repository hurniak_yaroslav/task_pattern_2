package com.statePattern.model.States.impl;

import com.statePattern.model.States.State;
import com.statePattern.model.Task;

public class ToDoState extends State {

    @Override
    public void toProgress(Task task) {
        task.setState(new InProgressState());
    }

}
