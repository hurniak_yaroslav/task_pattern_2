package com.statePattern.model.States.impl;

import com.statePattern.model.States.State;
import com.statePattern.model.Task;

public class CodeReviewState extends State {
    @Override
    public void addToDo(Task task) {
        task.setState(new ToDoState());
    }

    @Override
    public void toProgress(Task task) {
        task.setState(new InProgressState());
    }

}
